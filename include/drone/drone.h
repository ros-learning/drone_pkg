#pragma once

#include <stdlib.h> //to use random, for isntance
#include <time.h>       /* time */
#include <Eigen/Dense>
#include <chrono>
#include <thread>
// #include other files: #include "imu.h"

namespace acl {
namespace drone {

  class Drone
  {
  public:
    struct Params {
      int id_;
    };

  public:
    Drone(int id);
    ~Drone() = default; //destructor
    const Eigen::Vector3f& getLocation() { return location_; }
    const int& getID() { return params_.id_; }
    const int& getBattery() { return battery_; }
    void drawPower();
    void land();
    void flyUp();

  private:
    Params params_; ///< current parameters
    Eigen::Vector3f location_;
    int battery_;


  };

} // ns pendulum
} // ns acl
