
#pragma once

#include <cstdint>
#include <ros/ros.h>
#include <savvas_msgs/cmd.h>
#include <savvas_msgs/cmd_msg.h>
#include <savvas_msgs/State.h>
#include "drone/drone.h"

namespace acl {
namespace drone {

  class DroneROS
  {
    public:
      DroneROS(const ros::NodeHandle nh,int id);
      ~DroneROS() = default;

    public:
      void publishTimerCallback(const ros::TimerEvent& event);
      bool cmd(savvas_msgs::cmd::Request&, savvas_msgs::cmd::Response&);
      void cmdCallback(const savvas_msgs::cmd_msg& msg);

    private:

      ros::NodeHandle nh_;
      ros::Publisher pub_state_;
      ros::Subscriber sub_cmd_;
      ros::ServiceServer srv_cmd_;

      /// \brief Components
      Drone drone_; ///< Drone algorithm implementations

      /// \brief ROS callbacks

  };

} // ns pendulum
} // ns acl
