
#pragma once

#include <cstdint>
#include <ros/ros.h>
#include <savvas_msgs/cmd.h>
#include <savvas_msgs/State.h>
#include <savvas_msgs/cmd_msg.h>

namespace acl {
namespace gsc {

  class GSC_ROS
  {
    public:
      GSC_ROS(const ros::NodeHandle nh);
      ~GSC_ROS() = default;

    public:
      void stateCallback(const savvas_msgs::State& msg);
      // bool cmd(savvas_msgs::cmd::Request&, savvas_msgs::cmd::Response&);

    private:

      ros::NodeHandle nh_;
      ros::Subscriber sub_state_;
      ros::Publisher pub_cmd_;
      // ros::ServiceServer srv_cmd_;

  };

} // ns pendulum
} // ns acl
