
#include <ros/ros.h>
#include "drone/drone.h"


namespace acl {
namespace drone {

  Drone::Drone(int id)
{
  srand((unsigned) time(0));
  params_.id_ = id;
  battery_ = rand() % 51 + 50; // random nubber between 50 and 100;
  location_ << 0, params_.id_, 0;
}

void Drone::land(){
  location_ << location_(0), location_(1), 0.0;
}

void Drone::drawPower(){
  if ( location_(2) > 0 && battery_ > 0){
      battery_ -= 1;
  }
}

void Drone::flyUp(){
  location_ << location_(0), location_(1), 10.0;
}

} // ns drone
} // ns acl
