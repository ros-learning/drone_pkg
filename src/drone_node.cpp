#include <ros/ros.h> // define standard ros classses
#include "drone/drone_ros.h"



int main(int argc, char *argv[])
{
  ros::init(argc, argv, "drone"); //default name of node

  ros::NodeHandle nhtopics(""); //initial namespace will be : node_namespace/whatnot namespace instead of mode_namespace
  ros::NodeHandle nhptopics("~"); //initial namespace will be : node_namespace/whatnot namespace instead of mode_namespace
  int id;
  id = 0;

  //get the parameter
  std::string param;
  nhptopics.getParam("id", param);
  id = std::stoi(param.substr(1,param.size()-1));
  //start the node
  acl::drone::DroneROS node(nhtopics, id);
  //start the timer to regularly run the publish command
  ros::Timer processTimer = nhtopics.createTimer(ros::Duration(1.0), &acl::drone::DroneROS::publishTimerCallback, &node);
  ros::spin();
  return 0;
}
