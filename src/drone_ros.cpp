/**
 * @author Savva Morozov <savva@mit.edu>
 * @date 29 Dec 2019
 */

#include "drone/drone_ros.h"

namespace acl {
namespace drone {

DroneROS::DroneROS(const ros::NodeHandle nh, int id) : nh_(nh), drone_(id)
{
  //
  // Setup ROS communication
  //
  pub_state_ = nh_.advertise<savvas_msgs::State>("state", 1);
  sub_cmd_ = nh_.subscribe("cmd", 1, &DroneROS::cmdCallback, this);

  srv_cmd_ = nh_.advertiseService("drone" +std::to_string(id) + "/cmd", &DroneROS::cmd, this);

}

// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

void DroneROS::publishTimerCallback(const ros::TimerEvent& event)
{
  drone_.drawPower();
  { //publish drone state
    savvas_msgs::State msg;
    msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
    msg.header.frame_id = "drone";
    msg.loc.x = drone_.getLocation()(0);
    msg.loc.y = drone_.getLocation()(1);
    msg.loc.z = drone_.getLocation()(2);
    msg.id = drone_.getID();
    msg.battery =  drone_.getBattery();
    pub_state_.publish(msg);
  }
}


void DroneROS::cmdCallback(const savvas_msgs::cmd_msg& msg){
  if (msg.id == drone_.getID()){
    if (msg.flyUp){
      drone_.flyUp();
    }
    else{
      drone_.land();
    }
  }
}

bool DroneROS::cmd(savvas_msgs::cmd::Request &req,
                        savvas_msgs::cmd::Response &res)
{
  bool landing_requested = req.data;
  std::stringstream ss;
  ss << "Drone_" << drone_.getID();
  if (landing_requested) {
    drone_.land();
    ss << " landed.";
    res.message = ss.str();
  } else {
    drone_.flyUp();
    ss << " flew up.";
    res.message = ss.str();
  }
  res.success = true;
  return true;
}

} // ns drone
} // ns acl
