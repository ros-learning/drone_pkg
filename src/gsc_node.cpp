#include <ros/ros.h> // define standard ros classses
#include "drone/gsc_ros.h"

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "gsc"); //default name of node

  ros::NodeHandle nhtopics(""); //initial namespace will be : node_namespace/whatnot namespace instead of mode_namespace

  //start the node
  acl::gsc::GSC_ROS node(nhtopics);
  //start the timer to regularly run the publish command
  ros::spin();
  return 0;
}
