/**
 * @author Savva Morozov <savva@mit.edu>
 * @date 29 Dec 2019
 */

#include "drone/gsc_ros.h"

namespace acl {
namespace gsc {

GSC_ROS::GSC_ROS(const ros::NodeHandle nh) : nh_(nh)
{
  //
  // Setup ROS communication
  //
  sub_state_ = nh_.subscribe("state", 1000, &GSC_ROS::stateCallback, this);
  pub_cmd_ = nh_.advertise<savvas_msgs::cmd_msg>("cmd", 1000);



  // srv_cmd_ = nh_.advertiseService("cmd", &DroneROS::cmd, this);

}

// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

void GSC_ROS::stateCallback(const savvas_msgs::State& msg){
  if(msg.battery < 10 && msg.loc.z > 0){
    //land the drone it's running out of battery
    { //publish drone state
      savvas_msgs::cmd_msg new_msg;
      new_msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
      new_msg.header.frame_id = "gsc";
      new_msg.id = msg.id;
      new_msg.flyUp = false;
      pub_cmd_.publish(new_msg);
      ROS_INFO("drone_%i landed", msg.id);
    }

  }
  else if(msg.battery > 10 && !(msg.loc.z > 0) ){
    //got plenty of battery and on the ground - fly it
    { //publish drone state
      savvas_msgs::cmd_msg new_msg;
      new_msg.header.stamp = ros::Time::now(); // TODO: use time_us instead?
      new_msg.header.frame_id = "gsc";
      new_msg.id = msg.id;
      new_msg.flyUp = true;
      pub_cmd_.publish(new_msg);
      ROS_INFO("drone_%i flew up", msg.id);
    }
  }
}

// void GSC_ROS::serviceNode()
//
// bool DroneROS::cmd(savvas_msgs::cmd::Request &req,
//                         savvas_msgs::cmd::Response &res)
// {
//   bool landing_requested = req.data;
//   std::stringstream ss;
//   ss << "Drone_" << drone_.getID();
//   if (landing_requested) {
//     drone_.land();
//     ss << " landed.";
//     res.message = ss.str();
//   } else {
//     drone_.flyUp();
//     ss << " flew up.";
//     res.message = ss.str();
//   }
//   res.success = true;
//   return true;
// }

} // ns drone
} // ns acl
